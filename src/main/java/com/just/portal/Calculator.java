package com.just.portal;

public class Calculator {
    private int a;
    private int b;

    public Calculator(int a, int b) {
        super();
        this.a = a;
        this.b = b;
    }

    public int add(){
        return a+b;
    }

    public int sub(){
        return a- b;
    }

    public float div(){
        return (float)a/b;
    }

    public int mul(){
        return a*b;
    }
}
