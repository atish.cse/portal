package com.just.portal;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {
    
    @Test
    public void all_test(){
        Calculator calculator = new Calculator(5, 6);

        assertEquals(calculator.add(), 11);
        assertEquals(calculator.sub(), -1);
        assertEquals(calculator.mul(), 30);
    }
}
